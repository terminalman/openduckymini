EESchema Schematic File Version 2
LIBS:WhiteFox-rescue
LIBS:WhiteFox
LIBS:device
LIBS:WhiteFox-cache
EELAYER 25 0
EELAYER END
$Descr A3 16535 11693
encoding utf-8
Sheet 2 3
Title "WhiteFox"
Date "2016-06-11"
Rev "1.02a"
Comp "Input Club"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L C C7
U 1 1 4F22C606
P 9000 7450
F 0 "C7" H 8900 7550 50  0000 L CNN
F 1 "0.1uF" H 8800 7350 50  0000 L CNN
F 2 "lib.pretty:C_0805" V 8850 7450 50  0001 C CNN
F 3 "" H 9000 7450 60  0001 C CNN
	1    9000 7450
	-1   0    0    -1  
$EndComp
$Comp
L R R8
U 1 1 4E4186C6
P 8700 7850
F 0 "R8" V 8800 7850 50  0000 C CNN
F 1 "1k" V 8700 7850 50  0000 C CNN
F 2 "lib.pretty:R_0805" V 8600 7850 60  0001 C CNN
F 3 "" H 8700 7850 60  0001 C CNN
	1    8700 7850
	1    0    0    -1  
$EndComp
$Comp
L LED LED72
U 1 1 4E418482
P 8700 7350
F 0 "LED72" H 8700 7250 50  0000 C CNN
F 1 "LED" V 8800 7500 50  0001 C CNN
F 2 "lib.pretty:LED_0805" H 8700 7450 60  0001 C CNN
F 3 "" H 8700 7350 60  0001 C CNN
	1    8700 7350
	0    -1   1    0   
$EndComp
$Comp
L C C5
U 1 1 4E4181C7
P 11200 5550
F 0 "C5" H 11250 5650 50  0000 L CNN
F 1 "0.1uF" H 11250 5450 50  0000 L CNN
F 2 "lib.pretty:C_0805" V 11100 5550 60  0001 C CNN
F 3 "" H 11200 5550 60  0001 C CNN
	1    11200 5550
	1    0    0    -1  
$EndComp
$Comp
L C C4
U 1 1 4F22C5A2
P 6650 3800
F 0 "C4" H 6700 3900 50  0000 L CNN
F 1 "0.1uF" H 6700 3700 50  0000 L CNN
F 2 "lib.pretty:C_0805" V 6500 3800 50  0001 C CNN
F 3 "" H 6650 3800 60  0001 C CNN
	1    6650 3800
	-1   0    0    -1  
$EndComp
Text HLabel 8700 6800 3    40   3State ~ 0
PTA4
Text HLabel 10550 5150 2    40   3State ~ 0
PTB2
Text HLabel 10550 5050 2    40   3State ~ 0
PTB3
$Comp
L VSS #PWR01
U 1 1 50460275
P 6650 4100
F 0 "#PWR01" H 6650 4100 30  0001 C CNN
F 1 "VSS" H 6650 4030 30  0000 C CNN
F 2 "" H 6650 4100 60  0001 C CNN
F 3 "" H 6650 4100 60  0001 C CNN
	1    6650 4100
	1    0    0    -1  
$EndComp
$Comp
L VSS #PWR02
U 1 1 50460346
P 9000 7750
F 0 "#PWR02" H 9000 7750 30  0001 C CNN
F 1 "VSS" H 9000 7680 30  0000 C CNN
F 2 "" H 9000 7750 60  0001 C CNN
F 3 "" H 9000 7750 60  0001 C CNN
	1    9000 7750
	1    0    0    -1  
$EndComp
$Comp
L VSS #PWR03
U 1 1 504603CB
P 12200 5500
F 0 "#PWR03" H 12200 5500 30  0001 C CNN
F 1 "VSS" H 12200 5430 30  0000 C CNN
F 2 "" H 12200 5500 60  0001 C CNN
F 3 "" H 12200 5500 60  0001 C CNN
	1    12200 5500
	1    0    0    -1  
$EndComp
$Comp
L VSS #PWR04
U 1 1 50460578
P 11200 5800
F 0 "#PWR04" H 11200 5800 30  0001 C CNN
F 1 "VSS" H 11200 5730 30  0000 C CNN
F 2 "" H 11200 5800 60  0001 C CNN
F 3 "" H 11200 5800 60  0001 C CNN
	1    11200 5800
	1    0    0    -1  
$EndComp
$Comp
L VSS #PWR05
U 1 1 5047A8E0
P 8700 8250
F 0 "#PWR05" H 8700 8250 30  0001 C CNN
F 1 "VSS" H 8700 8180 30  0000 C CNN
F 2 "" H 8700 8250 60  0001 C CNN
F 3 "" H 8700 8250 60  0001 C CNN
	1    8700 8250
	1    0    0    -1  
$EndComp
NoConn ~ 11700 5250
Text HLabel 10550 4550 2    40   3State ~ 0
PTC0
Text HLabel 6550 4850 0    40   3State ~ 0
ADC0_DP0
Text HLabel 6550 4950 0    40   3State ~ 0
ADC0_DM0
$Comp
L VDD #PWR06
U 1 1 520A1E9B
P 6650 3550
F 0 "#PWR06" H 6650 3650 30  0001 C CNN
F 1 "VDD" H 6650 3660 30  0000 C CNN
F 2 "" H 6650 3550 60  0000 C CNN
F 3 "" H 6650 3550 60  0000 C CNN
	1    6650 3550
	1    0    0    -1  
$EndComp
$Comp
L VDD #PWR07
U 1 1 520A1ECD
P 8100 6600
F 0 "#PWR07" H 8100 6700 30  0001 C CNN
F 1 "VDD" H 8100 6710 30  0000 C CNN
F 2 "" H 8100 6600 60  0000 C CNN
F 3 "" H 8100 6600 60  0000 C CNN
	1    8100 6600
	1    0    0    -1  
$EndComp
$Comp
L VDD #PWR08
U 1 1 520A1EE4
P 9000 7200
F 0 "#PWR08" H 9000 7300 30  0001 C CNN
F 1 "VDD" H 9000 7310 30  0000 C CNN
F 2 "" H 9000 7200 60  0000 C CNN
F 3 "" H 9000 7200 60  0000 C CNN
	1    9000 7200
	1    0    0    -1  
$EndComp
$Comp
L R R2
U 1 1 539C9F35
P 4350 2950
F 0 "R2" V 4400 3150 50  0000 C CNN
F 1 "33" V 4350 2950 50  0000 C CNN
F 2 "lib.pretty:R_0805" V 4250 2950 50  0001 C CNN
F 3 "" H 4350 2950 60  0001 C CNN
	1    4350 2950
	0    -1   -1   0   
$EndComp
$Comp
L R R1
U 1 1 539C9F3B
P 4350 2850
F 0 "R1" V 4400 3050 50  0000 C CNN
F 1 "33" V 4350 2850 50  0000 C CNN
F 2 "lib.pretty:R_0805" V 4450 2850 50  0001 C CNN
F 3 "" H 4350 2850 60  0001 C CNN
	1    4350 2850
	0    -1   -1   0   
$EndComp
Text Label 4750 2850 0    39   ~ 0
USB_D-
Text Label 4750 2950 0    39   ~ 0
USB_D+
Text Label 6850 4450 2    39   ~ 0
USB_D+
Text Label 6850 4550 2    39   ~ 0
USB_D-
Text Label 8200 7650 3    39   ~ 0
SWD_CLK
Text Label 8500 7650 3    39   ~ 0
SWD_DIO
$Comp
L VSS #PWR09
U 1 1 539C9F4F
P 5100 3250
F 0 "#PWR09" H 5100 3250 30  0001 C CNN
F 1 "VSS" H 5100 3180 30  0000 C CNN
F 2 "" H 5100 3250 60  0000 C CNN
F 3 "" H 5100 3250 60  0000 C CNN
	1    5100 3250
	1    0    0    -1  
$EndComp
$Comp
L PWR_FLAG #FLG010
U 1 1 539D612B
P 4250 2650
F 0 "#FLG010" H 4250 2745 30  0001 C CNN
F 1 "PWR_FLAG" H 4250 2830 30  0000 C CNN
F 2 "" H 4250 2650 60  0000 C CNN
F 3 "" H 4250 2650 60  0000 C CNN
	1    4250 2650
	1    0    0    -1  
$EndComp
$Comp
L PWR_FLAG #FLG011
U 1 1 539D61F3
P 4850 3250
F 0 "#FLG011" H 4850 3345 30  0001 C CNN
F 1 "PWR_FLAG" H 4850 3430 30  0000 C CNN
F 2 "" H 4850 3250 60  0000 C CNN
F 3 "" H 4850 3250 60  0000 C CNN
	1    4850 3250
	-1   0    0    1   
$EndComp
NoConn ~ 8100 6300
NoConn ~ 8000 6300
NoConn ~ 7900 6300
NoConn ~ 7800 6300
$Comp
L C C3
U 1 1 54038768
P 10450 3600
F 0 "C3" H 10500 3700 50  0000 L CNN
F 1 "0.1uF" H 10500 3500 50  0000 L CNN
F 2 "lib.pretty:C_0805" V 10350 3600 50  0001 C CNN
F 3 "" H 10450 3600 60  0001 C CNN
	1    10450 3600
	1    0    0    -1  
$EndComp
$Comp
L VSS #PWR012
U 1 1 5403876E
P 10450 3900
F 0 "#PWR012" H 10450 3900 30  0001 C CNN
F 1 "VSS" H 10450 3830 30  0000 C CNN
F 2 "" H 10450 3900 60  0001 C CNN
F 3 "" H 10450 3900 60  0001 C CNN
	1    10450 3900
	-1   0    0    -1  
$EndComp
$Comp
L VDD #PWR013
U 1 1 5403877C
P 10450 3350
F 0 "#PWR013" H 10450 3450 30  0001 C CNN
F 1 "VDD" H 10450 3460 30  0000 C CNN
F 2 "" H 10450 3350 60  0000 C CNN
F 3 "" H 10450 3350 60  0000 C CNN
	1    10450 3350
	-1   0    0    -1  
$EndComp
Text HLabel 8900 6800 3    40   3State ~ 0
PTA12
Text HLabel 9000 6800 3    40   3State ~ 0
PTA13
Text HLabel 6550 5050 0    40   3State ~ 0
ADC1_DP0
Text HLabel 6550 5150 0    40   3State ~ 0
ADC1_DM0
Text HLabel 10550 4650 2    40   3State ~ 0
PTB19
Text HLabel 10550 4750 2    40   3State ~ 0
PTB18
Text HLabel 8600 2800 1    40   3State ~ 0
PTC11
Text HLabel 8800 2800 1    40   3State ~ 0
PTC9
Text HLabel 8700 2800 1    40   3State ~ 0
PTC10
Text Notes 11150 5050 0    40   ~ 0
RESET internally\npulled high
$Comp
L VSS #PWR014
U 1 1 549A599B
P 9700 6750
F 0 "#PWR014" H 9700 6750 30  0001 C CNN
F 1 "VSS" H 9700 6680 30  0000 C CNN
F 2 "~" H 9700 6750 60  0000 C CNN
F 3 "~" H 9700 6750 60  0000 C CNN
	1    9700 6750
	1    0    0    -1  
$EndComp
$Comp
L VSS #PWR015
U 1 1 549A59AA
P 9800 6750
F 0 "#PWR015" H 9800 6750 30  0001 C CNN
F 1 "VSS" H 9800 6680 30  0000 C CNN
F 2 "~" H 9800 6750 60  0000 C CNN
F 3 "~" H 9800 6750 60  0000 C CNN
	1    9800 6750
	1    0    0    -1  
$EndComp
$Comp
L CRYSTAL X1
U 1 1 549A5A7F
P 9750 6500
F 0 "X1" H 9750 6650 60  0000 C CNN
F 1 "CRYSTAL" H 10050 6450 60  0000 C CNN
F 2 "lib.pretty:NX3225SA" H 9750 6500 60  0001 C CNN
F 3 "~" H 9750 6500 60  0000 C CNN
F 4 "https://www.mouser.de/productdetail/abracon/abm8w-240000mhz-4-d1x-t3?qs=sGAEpiMZZMsBj6bBr9Q9abMK2mGYOCsJIL6S2X10%2FH0SAOeZi8mKSg%3D%3D" H 9750 6500 60  0001 C CNN "source"
	1    9750 6500
	1    0    0    -1  
$EndComp
Text HLabel 8500 2800 1    40   3State ~ 0
PTD0
Text HLabel 8400 2800 1    40   3State ~ 0
PTD1
Text HLabel 8100 2800 1    40   3State ~ 0
PTD4
Text HLabel 8000 2800 1    40   3State ~ 0
PTD5
Text HLabel 7900 2800 1    40   3State ~ 0
PTD6
Text HLabel 7800 2800 1    40   3State ~ 0
PTD7
Text Label 13300 4200 2    39   ~ 0
RX2
Text Label 13300 4300 2    39   ~ 0
TX2
Text Label 10550 5350 0    39   ~ 0
SCL0
Text Label 10550 5250 0    39   ~ 0
SDA0
Text Label 10550 4950 0    39   ~ 0
SDB
Text Label 10550 4850 0    39   ~ 0
INTB
Text Label 13300 4400 2    39   ~ 0
SDA0
Text Label 13300 4500 2    39   ~ 0
SCL0
$Comp
L TC1264 Q1
U 1 1 55226CE5
P 5700 2900
F 0 "Q1" H 5800 2800 60  0000 C CNN
F 1 "TC1264" H 5700 2900 60  0000 C CNN
F 2 "lib.pretty:SOT-223" H 5700 2900 60  0001 C CNN
F 3 "" H 5700 2900 60  0000 C CNN
F 4 "https://www.mouser.de/productdetail/microchip-technology/tc1264-33vdbtr?qs=sGAEpiMZZMsUzhEcHltCuTKAZwkZpPzk" H 5700 2900 60  0001 C CNN "source"
	1    5700 2900
	1    0    0    -1  
$EndComp
$Comp
L VDD #PWR016
U 1 1 552B5936
P 6650 2650
F 0 "#PWR016" H 6650 2750 30  0001 C CNN
F 1 "VDD" H 6650 2760 30  0000 C CNN
F 2 "" H 6650 2650 60  0000 C CNN
F 3 "" H 6650 2650 60  0000 C CNN
	1    6650 2650
	1    0    0    -1  
$EndComp
$Comp
L CT C2
U 1 1 552B5F6B
P 6650 2950
F 0 "C2" H 6650 3050 40  0000 L CNN
F 1 "1uF" H 6656 2865 40  0000 L CNN
F 2 "lib.pretty:C_0805" H 6688 2800 30  0001 C CNN
F 3 "" H 6650 2950 60  0000 C CNN
	1    6650 2950
	1    0    0    -1  
$EndComp
$Comp
L VSS #PWR017
U 1 1 552B6172
P 6650 3250
F 0 "#PWR017" H 6650 3250 30  0001 C CNN
F 1 "VSS" H 6650 3180 30  0000 C CNN
F 2 "" H 6650 3250 60  0000 C CNN
F 3 "" H 6650 3250 60  0000 C CNN
	1    6650 3250
	1    0    0    -1  
$EndComp
$Comp
L +5V #PWR018
U 1 1 539CD96A
P 4500 2650
F 0 "#PWR018" H 4500 2740 20  0001 C CNN
F 1 "+5V" H 4500 2740 30  0000 C CNN
F 2 "" H 4500 2650 60  0000 C CNN
F 3 "" H 4500 2650 60  0000 C CNN
	1    4500 2650
	1    0    0    -1  
$EndComp
Text Label 8800 7150 0    39   ~ 0
PTA5
Text Label 11200 5250 0    39   ~ 0
MCU_RESET
$Comp
L VSS #PWR019
U 1 1 55307C94
P 13000 4250
F 0 "#PWR019" H 13000 4250 30  0001 C CNN
F 1 "VSS" H 13000 4180 30  0000 C CNN
F 2 "" H 13000 4250 60  0000 C CNN
F 3 "" H 13000 4250 60  0000 C CNN
	1    13000 4250
	1    0    0    -1  
$EndComp
Text Label 13300 4000 2    39   ~ 0
SWD_CLK
Text Label 13300 3800 2    39   ~ 0
SWD_DIO
$Comp
L +5V #PWR020
U 1 1 55307C9C
P 13200 4100
F 0 "#PWR020" H 13200 4190 20  0001 C CNN
F 1 "+5V" H 13200 4190 30  0000 C CNN
F 2 "" H 13200 4100 60  0000 C CNN
F 3 "" H 13200 4100 60  0000 C CNN
	1    13200 4100
	1    0    0    -1  
$EndComp
Text Label 13300 4600 2    39   ~ 0
MCU_RESET
Text Label 13300 4700 2    39   ~ 0
AUD
$Comp
L CT C1
U 1 1 553097EE
P 5100 2950
F 0 "C1" H 5100 3050 40  0000 L CNN
F 1 "1uF" H 5106 2865 40  0000 L CNN
F 2 "lib.pretty:C_0805" H 5138 2800 30  0001 C CNN
F 3 "" H 5100 2950 60  0000 C CNN
	1    5100 2950
	1    0    0    -1  
$EndComp
$Comp
L PWR_FLAG #FLG021
U 1 1 5530A596
P 6350 2650
F 0 "#FLG021" H 6350 2745 30  0001 C CNN
F 1 "PWR_FLAG" H 6350 2830 30  0000 C CNN
F 2 "" H 6350 2650 60  0000 C CNN
F 3 "" H 6350 2650 60  0000 C CNN
	1    6350 2650
	1    0    0    -1  
$EndComp
$Comp
L R R4
U 1 1 5530CD63
P 8500 7400
F 0 "R4" V 8400 7400 50  0000 C CNN
F 1 "100" V 8500 7400 50  0000 C CNN
F 2 "lib.pretty:R_0805" V 8400 7400 60  0001 C CNN
F 3 "" H 8500 7400 60  0001 C CNN
	1    8500 7400
	1    0    0    -1  
$EndComp
$Comp
L R R3
U 1 1 5530D006
P 8200 7400
F 0 "R3" V 8100 7400 50  0000 C CNN
F 1 "100" V 8200 7400 50  0000 C CNN
F 2 "lib.pretty:R_0805" V 8100 7400 60  0001 C CNN
F 3 "" H 8200 7400 60  0001 C CNN
	1    8200 7400
	1    0    0    -1  
$EndComp
NoConn ~ 7050 5250
NoConn ~ 7050 5350
NoConn ~ 7050 5450
NoConn ~ 7050 5550
$Comp
L TAC_SWITCH S72
U 1 1 5533596A
P 11900 5150
F 0 "S72" H 11875 5375 50  0000 L BNN
F 1 "TAC_SWITCH" H 11675 4950 50  0000 L BNN
F 2 "lib.pretty:TACT_5.2x5.2" H 11900 5300 50  0001 C CNN
F 3 "" H 11900 5150 60  0000 C CNN
F 4 "owned" H 11900 5150 60  0001 C CNN "source"
	1    11900 5150
	1    0    0    -1  
$EndComp
Text HLabel 9300 2800 1    40   3State ~ 0
PTC4
Text HLabel 9200 2800 1    40   3State ~ 0
PTC5
Text HLabel 9100 2800 1    40   3State ~ 0
PTC6
Text HLabel 9000 2800 1    40   3State ~ 0
PTC7
Text HLabel 8900 2800 1    40   3State ~ 0
PTC8
Text HLabel 10550 4450 2    40   3State ~ 0
PTC1
Text HLabel 10550 4350 2    40   3State ~ 0
PTC2
Text HLabel 10550 4250 2    40   3State ~ 0
PTC3
$Comp
L CONN_11 P2
U 1 1 55407697
P 13600 4200
F 0 "P2" H 13600 3600 59  0000 C CNN
F 1 "CONN_11" V 13600 4250 59  0000 C CNN
F 2 "lib.pretty:HEADER_1x11" H 13600 4250 60  0001 C CNN
F 3 "" H 13600 4250 60  0000 C CNN
F 4 "Single-row" H 13520 3770 5   0001 L BNN "Field4"
	1    13600 4200
	1    0    0    -1  
$EndComp
Text Label 8200 2800 1    39   ~ 0
TX2
Text Label 8300 2800 1    39   ~ 0
RX2
NoConn ~ 7050 4050
NoConn ~ 7050 4150
$Comp
L VDD #PWR022
U 1 1 556B5968
P 6300 4600
F 0 "#PWR022" H 6300 4700 30  0001 C CNN
F 1 "VDD" H 6300 4710 30  0000 C CNN
F 2 "" H 6300 4600 60  0000 C CNN
F 3 "" H 6300 4600 60  0000 C CNN
	1    6300 4600
	1    0    0    -1  
$EndComp
$Comp
L MK20DX256VLH7 U1
U 1 1 54037CD8
P 8550 4800
F 0 "U1" H 8550 5050 97  0000 C CNN
F 1 "MK20DX256VLH7" H 8550 4900 97  0000 C CNN
F 2 "lib.pretty:LQFP64" H 8550 4800 60  0001 C CNN
F 3 "~" H 8550 4800 60  0000 C CNN
F 4 "owned" H 8550 4800 60  0001 C CNN "source"
	1    8550 4800
	1    0    0    -1  
$EndComp
Text Label 8400 6300 3    39   ~ 0
RX0
Text Label 8500 6300 3    39   ~ 0
TX0
$Comp
L USB_CONN J2
U 1 1 57ECE760
P 3050 2950
F 0 "J2" V 3000 2950 50  0000 C CNN
F 1 "USB_CONN" V 3100 2950 50  0000 C CNN
F 2 "lib.pretty:MOLEX_PICO_5PIN" H 3050 2950 60  0001 C CNN
F 3 "" H 3050 2950 60  0000 C CNN
	1    3050 2950
	-1   0    0    -1  
$EndComp
NoConn ~ 3050 3500
NoConn ~ 3450 3050
Text Label 4050 2850 0    60   ~ 0
D-
Text Label 4050 2950 0    60   ~ 0
D+
$Comp
L IS31FL3733 U2
U 1 1 581042DA
P 4500 8350
AR Path="/581042DA" Ref="U2"  Part="1" 
AR Path="/549878F0/581042DA" Ref="U2"  Part="1" 
F 0 "U2" H 4550 8500 60  0000 C CNN
F 1 "IS31FL3733" H 4500 8300 60  0000 C CNN
F 2 "lib.pretty:QFN-48-1EP_6x6mm_Pitch0.4mm" H 5100 7500 60  0001 C CNN
F 3 "" H 5100 7500 60  0001 C CNN
F 4 "https://www.mouser.de/productdetail/issi/is31fl3733-tqls4?qs=sGAEpiMZZMsPdFgpQMKDR3OroQyQ1gJj531%2FGuE6evgb2gMRRhjQTQ%3D%3D" H 4500 8350 60  0001 C CNN "source"
	1    4500 8350
	1    0    0    -1  
$EndComp
$Comp
L VSS #PWR023
U 1 1 581068E0
P 3600 6500
F 0 "#PWR023" H 3600 6500 30  0001 C CNN
F 1 "VSS" H 3600 6430 30  0000 C CNN
F 2 "~" H 3600 6500 60  0000 C CNN
F 3 "~" H 3600 6500 60  0000 C CNN
	1    3600 6500
	1    0    0    1   
$EndComp
$Comp
L VSS #PWR024
U 1 1 581069FB
P 3300 6500
F 0 "#PWR024" H 3300 6500 30  0001 C CNN
F 1 "VSS" H 3300 6430 30  0000 C CNN
F 2 "~" H 3300 6500 60  0000 C CNN
F 3 "~" H 3300 6500 60  0000 C CNN
	1    3300 6500
	1    0    0    1   
$EndComp
$Comp
L VSS #PWR025
U 1 1 58107875
P 3600 7700
F 0 "#PWR025" H 3600 7700 30  0001 C CNN
F 1 "VSS" H 3600 7630 30  0000 C CNN
F 2 "~" H 3600 7700 60  0000 C CNN
F 3 "~" H 3600 7700 60  0000 C CNN
	1    3600 7700
	-1   0    0    -1  
$EndComp
$Comp
L VSS #PWR026
U 1 1 58107BB0
P 3300 7700
F 0 "#PWR026" H 3300 7700 30  0001 C CNN
F 1 "VSS" H 3300 7630 30  0000 C CNN
F 2 "~" H 3300 7700 60  0000 C CNN
F 3 "~" H 3300 7700 60  0000 C CNN
	1    3300 7700
	-1   0    0    -1  
$EndComp
$Comp
L VSS #PWR027
U 1 1 58107FF6
P 3100 7350
F 0 "#PWR027" H 3100 7350 30  0001 C CNN
F 1 "VSS" H 3100 7280 30  0000 C CNN
F 2 "~" H 3100 7350 60  0000 C CNN
F 3 "~" H 3100 7350 60  0000 C CNN
	1    3100 7350
	-1   0    0    -1  
$EndComp
$Comp
L VSS #PWR028
U 1 1 58107FFC
P 2800 7350
F 0 "#PWR028" H 2800 7350 30  0001 C CNN
F 1 "VSS" H 2800 7280 30  0000 C CNN
F 2 "~" H 2800 7350 60  0000 C CNN
F 3 "~" H 2800 7350 60  0000 C CNN
	1    2800 7350
	-1   0    0    -1  
$EndComp
$Comp
L VSS #PWR029
U 1 1 5810D0B4
P 2400 7300
F 0 "#PWR029" H 2400 7300 30  0001 C CNN
F 1 "VSS" H 2400 7230 30  0000 C CNN
F 2 "~" H 2400 7300 60  0000 C CNN
F 3 "~" H 2400 7300 60  0000 C CNN
	1    2400 7300
	-1   0    0    -1  
$EndComp
Text Label 1950 8100 2    39   ~ 0
SDA0
Text Label 1950 8250 2    39   ~ 0
SCL0
Text Label 1950 8400 2    39   ~ 0
INTB
Text Label 1950 8550 2    39   ~ 0
SDB
$Comp
L R R5
U 1 1 58111D9D
P 2000 7800
F 0 "R5" V 2080 7800 40  0000 C CNN
F 1 "100k" V 2007 7801 40  0000 C CNN
F 2 "lib.pretty:R_0805" V 1930 7800 30  0001 C CNN
F 3 "~" H 2000 7800 30  0000 C CNN
	1    2000 7800
	-1   0    0    -1  
$EndComp
$Comp
L R R6
U 1 1 58111DA3
P 2150 7800
F 0 "R6" V 2230 7800 40  0000 C CNN
F 1 "4.7k" V 2157 7801 40  0000 C CNN
F 2 "lib.pretty:R_0805" V 2080 7800 30  0001 C CNN
F 3 "~" H 2150 7800 30  0000 C CNN
	1    2150 7800
	-1   0    0    -1  
$EndComp
$Comp
L R R7
U 1 1 58111DA9
P 2300 7800
F 0 "R7" V 2380 7800 40  0000 C CNN
F 1 "4.7k" V 2307 7801 40  0000 C CNN
F 2 "lib.pretty:R_0805" V 2230 7800 30  0001 C CNN
F 3 "~" H 2300 7800 30  0000 C CNN
	1    2300 7800
	-1   0    0    -1  
$EndComp
$Comp
L VDD #PWR030
U 1 1 581178B5
P 2000 7550
F 0 "#PWR030" H 2000 7650 30  0001 C CNN
F 1 "VDD" H 2000 7660 30  0000 C CNN
F 2 "" H 2000 7550 60  0000 C CNN
F 3 "" H 2000 7550 60  0000 C CNN
	1    2000 7550
	-1   0    0    -1  
$EndComp
$Comp
L VDD #PWR031
U 1 1 581178E5
P 2150 7550
F 0 "#PWR031" H 2150 7650 30  0001 C CNN
F 1 "VDD" H 2150 7660 30  0000 C CNN
F 2 "" H 2150 7550 60  0000 C CNN
F 3 "" H 2150 7550 60  0000 C CNN
	1    2150 7550
	-1   0    0    -1  
$EndComp
$Comp
L VDD #PWR032
U 1 1 58117915
P 2300 7550
F 0 "#PWR032" H 2300 7650 30  0001 C CNN
F 1 "VDD" H 2300 7660 30  0000 C CNN
F 2 "" H 2300 7550 60  0000 C CNN
F 3 "" H 2300 7550 60  0000 C CNN
	1    2300 7550
	-1   0    0    -1  
$EndComp
$Comp
L R R9
U 1 1 58118C30
P 3300 8950
F 0 "R9" V 3380 8950 40  0000 C CNN
F 1 "100k" V 3307 8951 40  0000 C CNN
F 2 "lib.pretty:R_0805" V 3230 8950 30  0001 C CNN
F 3 "~" H 3300 8950 30  0000 C CNN
	1    3300 8950
	-1   0    0    -1  
$EndComp
$Comp
L VSS #PWR033
U 1 1 58118C36
P 3300 9200
F 0 "#PWR033" H 3300 9200 30  0001 C CNN
F 1 "VSS" H 3300 9130 30  0000 C CNN
F 2 "~" H 3300 9200 60  0000 C CNN
F 3 "~" H 3300 9200 60  0000 C CNN
	1    3300 9200
	-1   0    0    -1  
$EndComp
$Comp
L R R10
U 1 1 58119BBF
P 3550 8950
F 0 "R10" V 3630 8950 40  0000 C CNN
F 1 "100k" V 3557 8951 40  0000 C CNN
F 2 "lib.pretty:R_0805" V 3480 8950 30  0001 C CNN
F 3 "~" H 3550 8950 30  0000 C CNN
	1    3550 8950
	-1   0    0    -1  
$EndComp
$Comp
L VSS #PWR034
U 1 1 58119BC5
P 3550 9200
F 0 "#PWR034" H 3550 9200 30  0001 C CNN
F 1 "VSS" H 3550 9130 30  0000 C CNN
F 2 "~" H 3550 9200 60  0000 C CNN
F 3 "~" H 3550 9200 60  0000 C CNN
	1    3550 9200
	-1   0    0    -1  
$EndComp
$Comp
L VSS #PWR035
U 1 1 5811A2F4
P 3750 10100
F 0 "#PWR035" H 3750 10100 30  0001 C CNN
F 1 "VSS" H 3750 10030 30  0000 C CNN
F 2 "~" H 3750 10100 60  0000 C CNN
F 3 "~" H 3750 10100 60  0000 C CNN
	1    3750 10100
	0    1    -1   0   
$EndComp
$Comp
L VSS #PWR036
U 1 1 5811A59D
P 3250 9650
F 0 "#PWR036" H 3250 9650 30  0001 C CNN
F 1 "VSS" H 3250 9580 30  0000 C CNN
F 2 "~" H 3250 9650 60  0000 C CNN
F 3 "~" H 3250 9650 60  0000 C CNN
	1    3250 9650
	0    1    -1   0   
$EndComp
$Comp
L R R11
U 1 1 5811A5EF
P 3500 9650
F 0 "R11" V 3580 9650 40  0000 C CNN
F 1 "20k" V 3507 9651 40  0000 C CNN
F 2 "lib.pretty:R_0805" V 3430 9650 30  0001 C CNN
F 3 "~" H 3500 9650 30  0000 C CNN
	1    3500 9650
	0    1    -1   0   
$EndComp
$Comp
L VSS #PWR037
U 1 1 5811CC8F
P 5300 9950
F 0 "#PWR037" H 5300 9950 30  0001 C CNN
F 1 "VSS" H 5300 9880 30  0000 C CNN
F 2 "~" H 5300 9950 60  0000 C CNN
F 3 "~" H 5300 9950 60  0000 C CNN
	1    5300 9950
	0    -1   1    0   
$EndComp
$Comp
L VSS #PWR038
U 1 1 5811CDA5
P 5300 10100
F 0 "#PWR038" H 5300 10100 30  0001 C CNN
F 1 "VSS" H 5300 10030 30  0000 C CNN
F 2 "~" H 5300 10100 60  0000 C CNN
F 3 "~" H 5300 10100 60  0000 C CNN
	1    5300 10100
	0    -1   1    0   
$EndComp
$Comp
L VDD #PWR039
U 1 1 5811D96C
P 6000 6800
F 0 "#PWR039" H 6000 6900 30  0001 C CNN
F 1 "VDD" H 6000 6910 30  0000 C CNN
F 2 "" H 6000 6800 60  0000 C CNN
F 3 "" H 6000 6800 60  0000 C CNN
	1    6000 6800
	-1   0    0    -1  
$EndComp
$Comp
L VSS #PWR040
U 1 1 5811DCBB
P 6000 7250
F 0 "#PWR040" H 6000 7250 30  0001 C CNN
F 1 "VSS" H 6000 7180 30  0000 C CNN
F 2 "~" H 6000 7250 60  0000 C CNN
F 3 "~" H 6000 7250 60  0000 C CNN
	1    6000 7250
	-1   0    0    -1  
$EndComp
Text HLabel 5300 9750 2    60   Input ~ 0
SW1
Text HLabel 5300 9650 2    60   Input ~ 0
SW2
Text HLabel 5300 9550 2    60   Input ~ 0
SW3
Text HLabel 5300 9450 2    60   Input ~ 0
SW4
Text HLabel 5300 9350 2    60   Input ~ 0
SW5
Text HLabel 5300 9250 2    60   Input ~ 0
SW6
Text HLabel 5300 9150 2    60   Input ~ 0
SW7
Text HLabel 5300 9050 2    60   Input ~ 0
SW8
Text HLabel 5300 8950 2    60   Input ~ 0
SW9
Text HLabel 5300 8850 2    60   Input ~ 0
SW10
Text HLabel 5300 8750 2    60   Input ~ 0
SW11
Text HLabel 5300 8650 2    60   Input ~ 0
SW12
Text HLabel 5300 8500 2    60   Input ~ 0
CS1
Text HLabel 5300 8400 2    60   Input ~ 0
CS2
Text HLabel 5300 8300 2    60   Input ~ 0
CS3
Text HLabel 5300 8200 2    60   Input ~ 0
CS4
Text HLabel 5300 8100 2    60   Input ~ 0
CS5
Text HLabel 5300 8000 2    60   Input ~ 0
CS6
Text HLabel 5300 7900 2    60   Input ~ 0
CS7
Text HLabel 5300 7800 2    60   Input ~ 0
CS8
Text HLabel 5300 7700 2    60   Input ~ 0
CS9
Text HLabel 5300 7600 2    60   Input ~ 0
CS10
Text HLabel 5300 7500 2    60   Input ~ 0
CS11
Text HLabel 5300 7400 2    60   Input ~ 0
CS12
Text HLabel 5300 7300 2    60   Input ~ 0
CS13
Text HLabel 5300 7200 2    60   Input ~ 0
CS14
Text HLabel 5300 7100 2    60   Input ~ 0
CS15
Text HLabel 5300 7000 2    60   Input ~ 0
CS16
$Comp
L VSS #PWR041
U 1 1 58126407
P 3750 9950
F 0 "#PWR041" H 3750 9950 30  0001 C CNN
F 1 "VSS" H 3750 9880 30  0000 C CNN
F 2 "~" H 3750 9950 60  0000 C CNN
F 3 "~" H 3750 9950 60  0000 C CNN
	1    3750 9950
	0    1    -1   0   
$EndComp
$Comp
L VSS #PWR042
U 1 1 5812643B
P 3750 9800
F 0 "#PWR042" H 3750 9800 30  0001 C CNN
F 1 "VSS" H 3750 9730 30  0000 C CNN
F 2 "~" H 3750 9800 60  0000 C CNN
F 3 "~" H 3750 9800 60  0000 C CNN
	1    3750 9800
	0    1    -1   0   
$EndComp
$Comp
L VSS #PWR043
U 1 1 58133999
P 4500 10350
F 0 "#PWR043" H 4500 10350 30  0001 C CNN
F 1 "VSS" H 4500 10280 30  0000 C CNN
F 2 "~" H 4500 10350 60  0000 C CNN
F 3 "~" H 4500 10350 60  0000 C CNN
	1    4500 10350
	-1   0    0    -1  
$EndComp
$Comp
L +5V #PWR044
U 1 1 5A33D176
P 2800 6850
F 0 "#PWR044" H 2800 6940 20  0001 C CNN
F 1 "+5V" H 2800 6940 30  0000 C CNN
F 2 "" H 2800 6850 60  0000 C CNN
F 3 "" H 2800 6850 60  0000 C CNN
	1    2800 6850
	1    0    0    -1  
$EndComp
$Comp
L +5V #PWR045
U 1 1 5A33D19F
P 2400 7000
F 0 "#PWR045" H 2400 7090 20  0001 C CNN
F 1 "+5V" H 2400 7090 30  0000 C CNN
F 2 "" H 2400 7000 60  0000 C CNN
F 3 "" H 2400 7000 60  0000 C CNN
	1    2400 7000
	1    0    0    -1  
$EndComp
$Comp
L +5V #PWR046
U 1 1 5A33D1CA
P 3300 7300
F 0 "#PWR046" H 3300 7390 20  0001 C CNN
F 1 "+5V" H 3300 7390 30  0000 C CNN
F 2 "" H 3300 7300 60  0000 C CNN
F 3 "" H 3300 7300 60  0000 C CNN
	1    3300 7300
	1    0    0    -1  
$EndComp
$Comp
L VDD #PWR047
U 1 1 55307CA2
P 13200 3550
F 0 "#PWR047" H 13200 3650 30  0001 C CNN
F 1 "VDD" H 13200 3660 30  0000 C CNN
F 2 "" H 13200 3550 60  0000 C CNN
F 3 "" H 13200 3550 60  0000 C CNN
	1    13200 3550
	1    0    0    -1  
$EndComp
$Comp
L PTC_FUSE U3
U 1 1 5A534BDE
P 3850 2750
F 0 "U3" H 3850 2950 60  0000 C CNN
F 1 "PTC_FUSE" H 3850 2850 60  0000 C CNN
F 2 "lib.pretty:PTC_1206" H 3850 2850 60  0001 C CNN
F 3 "" H 3850 2850 60  0001 C CNN
F 4 "https://www.digikey.com/product-detail/en/bel-fuse-inc/0ZCJ0050FF2G/507-1802-2-ND/4156133" H 3850 2750 60  0001 C CNN "source"
	1    3850 2750
	1    0    0    -1  
$EndComp
$Comp
L CP C10
U 1 1 5A5C7FE5
P 2400 7150
F 0 "C10" H 2425 7250 50  0000 L CNN
F 1 "22uF" H 2425 7050 50  0000 L CNN
F 2 "lib.pretty:EEF-CD0J220R" H 2438 7000 50  0001 C CNN
F 3 "" H 2400 7150 50  0001 C CNN
F 4 "https://www.mouser.de/productdetail/panasonic/eef-cd0j220r?qs=sGAEpiMZZMsIwzbKW1rlgXr%2feIyT5o4TgEU3ivkAKEI=" H 2400 7150 60  0001 C CNN "source"
	1    2400 7150
	1    0    0    -1  
$EndComp
$Comp
L CP C11
U 1 1 5A5CAA70
P 2800 7200
F 0 "C11" H 2825 7300 50  0000 L CNN
F 1 "1uF" H 2825 7100 50  0000 L CNN
F 2 "Capacitors_Tantalum_SMD:CP_Tantalum_Case-R_EIA-2012-12_Reflow" H 2838 7050 50  0001 C CNN
F 3 "" H 2800 7200 50  0001 C CNN
	1    2800 7200
	1    0    0    -1  
$EndComp
Wire Wire Line
	8200 3300 8200 2800
Wire Wire Line
	8400 3300 8400 2800
Wire Wire Line
	9800 6650 9800 6750
Wire Wire Line
	9700 6650 9700 6750
Wire Wire Line
	9300 6500 9450 6500
Wire Wire Line
	9300 6300 9300 6500
Wire Wire Line
	10050 5550 10050 6500
Wire Wire Line
	8800 6300 8800 7150
Wire Wire Line
	8800 3300 8800 2800
Wire Wire Line
	8700 2800 8700 3300
Wire Wire Line
	8600 3300 8600 2800
Wire Wire Line
	8900 3300 8900 2800
Wire Wire Line
	10550 4750 10050 4750
Wire Wire Line
	10550 4650 10050 4650
Wire Wire Line
	7050 5150 6550 5150
Wire Wire Line
	7050 5050 6550 5050
Wire Wire Line
	9000 6300 9000 6800
Wire Wire Line
	8900 6300 8900 6800
Wire Wire Line
	10200 3400 10450 3400
Wire Wire Line
	10200 4050 10200 3400
Wire Wire Line
	10050 4050 10200 4050
Wire Wire Line
	10300 4150 10050 4150
Wire Wire Line
	10300 3800 10300 4150
Wire Wire Line
	10450 3800 10300 3800
Wire Wire Line
	10450 3400 10450 3350
Wire Wire Line
	10450 3900 10450 3800
Wire Wire Line
	8500 3300 8500 2800
Wire Wire Line
	4850 3250 4850 3150
Connection ~ 4850 3150
Wire Wire Line
	9200 7650 9200 6300
Wire Wire Line
	9000 7650 9200 7650
Wire Wire Line
	4250 2650 4250 2750
Wire Wire Line
	10550 4950 10050 4950
Wire Wire Line
	7050 4450 6850 4450
Wire Wire Line
	7050 4550 6850 4550
Wire Wire Line
	4600 2850 4750 2850
Wire Wire Line
	4600 2950 4750 2950
Wire Wire Line
	10550 4550 10050 4550
Wire Wire Line
	9200 3300 9200 2800
Wire Wire Line
	9100 2800 9100 3300
Wire Wire Line
	9000 3300 9000 2800
Wire Wire Line
	10550 4850 10050 4850
Wire Wire Line
	10050 4450 10550 4450
Wire Wire Line
	6900 3600 6650 3600
Wire Wire Line
	6900 4250 6900 3600
Wire Wire Line
	7050 4250 6900 4250
Wire Wire Line
	6800 4350 7050 4350
Wire Wire Line
	6800 4000 6800 4350
Wire Wire Line
	6650 4000 6800 4000
Connection ~ 11200 5250
Wire Wire Line
	11700 5150 11200 5150
Wire Wire Line
	11200 5150 11200 5350
Wire Wire Line
	8700 7550 8700 7600
Wire Wire Line
	8700 8100 8700 8250
Wire Wire Line
	8100 3300 8100 2800
Wire Wire Line
	12100 5250 12100 5150
Wire Wire Line
	6650 3600 6650 3550
Wire Wire Line
	9000 7250 9100 7250
Wire Wire Line
	9000 7200 9000 7250
Wire Wire Line
	10550 5050 10050 5050
Wire Wire Line
	10050 5150 10550 5150
Wire Wire Line
	10550 5250 10050 5250
Wire Wire Line
	10050 5350 10550 5350
Wire Wire Line
	10550 4350 10050 4350
Wire Wire Line
	10050 4250 10550 4250
Wire Wire Line
	9300 3300 9300 2800
Wire Wire Line
	8000 2800 8000 3300
Wire Wire Line
	7900 3300 7900 2800
Wire Wire Line
	7800 3300 7800 2800
Wire Wire Line
	8700 6800 8700 6300
Wire Wire Line
	8600 6300 8600 7150
Wire Wire Line
	9000 7650 9000 7750
Wire Wire Line
	9100 7250 9100 6300
Wire Wire Line
	7050 4950 6550 4950
Wire Wire Line
	7050 4850 6550 4850
Wire Wire Line
	6650 4100 6650 4000
Wire Wire Line
	11200 5750 11200 5800
Wire Wire Line
	12200 5250 12100 5250
Wire Wire Line
	12200 5500 12200 5250
Wire Wire Line
	8300 2800 8300 3300
Wire Wire Line
	10050 5450 11050 5450
Wire Wire Line
	11050 5450 11050 5250
Wire Wire Line
	11050 5250 11200 5250
Connection ~ 5100 3150
Wire Wire Line
	6650 3250 6650 3150
Connection ~ 6650 2750
Wire Wire Line
	4500 2750 4500 2650
Wire Wire Line
	13300 3700 13200 3700
Wire Wire Line
	13200 3700 13200 3550
Wire Wire Line
	13300 3900 13000 3900
Wire Wire Line
	13000 3900 13000 4250
Wire Wire Line
	13300 4100 13200 4100
Wire Wire Line
	5250 3150 5250 3000
Wire Wire Line
	5100 3150 5100 3250
Wire Wire Line
	6100 2750 6650 2750
Connection ~ 6350 2750
Wire Wire Line
	6650 2750 6650 2650
Wire Wire Line
	6350 2750 6350 2650
Wire Wire Line
	8200 6300 8200 6650
Wire Wire Line
	8200 6650 8100 6650
Wire Wire Line
	8100 6650 8100 6600
Wire Wire Line
	8800 7150 8700 7150
Wire Wire Line
	8600 7150 8500 7150
Wire Wire Line
	8300 6300 8300 7150
Wire Wire Line
	8300 7150 8200 7150
Wire Wire Line
	6300 4600 6300 4650
Wire Wire Line
	6300 4650 7050 4650
Wire Wire Line
	6850 4650 6850 4750
Wire Wire Line
	6850 4750 7050 4750
Connection ~ 6850 4650
Wire Wire Line
	3300 7350 3750 7350
Connection ~ 3600 7350
Wire Wire Line
	2800 7000 3750 7000
Connection ~ 3100 7000
Wire Wire Line
	2800 6850 2800 7050
Wire Wire Line
	1950 8250 3750 8250
Wire Wire Line
	1950 8400 3750 8400
Wire Wire Line
	1950 8550 3750 8550
Wire Wire Line
	3750 8100 1950 8100
Wire Wire Line
	2300 8050 2300 8100
Connection ~ 2300 8100
Wire Wire Line
	2150 8050 2150 8250
Connection ~ 2150 8250
Wire Wire Line
	2000 8050 2000 8400
Connection ~ 2000 8400
Wire Wire Line
	3300 8550 3300 8700
Wire Wire Line
	3550 8700 3750 8700
Connection ~ 3300 8550
Wire Wire Line
	5300 6850 6000 6850
Wire Wire Line
	6000 6800 6000 6950
Wire Wire Line
	3300 7300 3300 7400
Wire Wire Line
	2800 6850 3750 6850
Connection ~ 3300 6850
Connection ~ 3600 6850
Connection ~ 6000 6850
Wire Wire Line
	4150 2750 5250 2750
Connection ~ 4250 2750
Connection ~ 4500 2750
Connection ~ 5100 2750
Wire Wire Line
	3450 2750 3550 2750
Wire Wire Line
	3450 2850 4100 2850
Wire Wire Line
	3450 2950 4100 2950
Wire Wire Line
	3450 3150 5250 3150
Connection ~ 2800 7000
$Comp
L CP C12
U 1 1 5A5CC394
P 3100 7200
F 0 "C12" H 3125 7300 50  0000 L CNN
F 1 "1uF" H 3125 7100 50  0000 L CNN
F 2 "Capacitors_Tantalum_SMD:CP_Tantalum_Case-R_EIA-2012-12_Reflow" H 3138 7050 50  0001 C CNN
F 3 "" H 3100 7200 50  0001 C CNN
	1    3100 7200
	1    0    0    -1  
$EndComp
Wire Wire Line
	3100 7000 3100 7050
$Comp
L CP C13
U 1 1 5A5CD208
P 3300 7550
F 0 "C13" H 3325 7650 50  0000 L CNN
F 1 "1uF" H 3325 7450 50  0000 L CNN
F 2 "Capacitors_Tantalum_SMD:CP_Tantalum_Case-R_EIA-2012-12_Reflow" H 3338 7400 50  0001 C CNN
F 3 "" H 3300 7550 50  0001 C CNN
	1    3300 7550
	1    0    0    -1  
$EndComp
$Comp
L CP C14
U 1 1 5A5CD356
P 3600 7550
F 0 "C14" H 3625 7650 50  0000 L CNN
F 1 "1uF" H 3625 7450 50  0000 L CNN
F 2 "Capacitors_Tantalum_SMD:CP_Tantalum_Case-R_EIA-2012-12_Reflow" H 3638 7400 50  0001 C CNN
F 3 "" H 3600 7550 50  0001 C CNN
	1    3600 7550
	1    0    0    -1  
$EndComp
Connection ~ 3300 7350
Wire Wire Line
	3600 7350 3600 7400
$Comp
L CP C6
U 1 1 5A5CFB42
P 3300 6650
F 0 "C6" H 3325 6750 50  0000 L CNN
F 1 "1uF" H 3325 6550 50  0000 L CNN
F 2 "Capacitors_Tantalum_SMD:CP_Tantalum_Case-R_EIA-2012-12_Reflow" H 3338 6500 50  0001 C CNN
F 3 "" H 3300 6650 50  0001 C CNN
	1    3300 6650
	-1   0    0    1   
$EndComp
$Comp
L CP C8
U 1 1 5A5CFD2A
P 3600 6650
F 0 "C8" H 3625 6750 50  0000 L CNN
F 1 "1uF" H 3625 6550 50  0000 L CNN
F 2 "Capacitors_Tantalum_SMD:CP_Tantalum_Case-R_EIA-2012-12_Reflow" H 3638 6500 50  0001 C CNN
F 3 "" H 3600 6650 50  0001 C CNN
	1    3600 6650
	-1   0    0    1   
$EndComp
Wire Wire Line
	3600 6850 3600 6800
Wire Wire Line
	3300 6850 3300 6800
$Comp
L CP C9
U 1 1 5A5D0EA3
P 6000 7100
F 0 "C9" H 6025 7200 50  0000 L CNN
F 1 "1uF" H 6025 7000 50  0000 L CNN
F 2 "Capacitors_Tantalum_SMD:CP_Tantalum_Case-R_EIA-2012-12_Reflow" H 6038 6950 50  0001 C CNN
F 3 "" H 6000 7100 50  0001 C CNN
	1    6000 7100
	1    0    0    -1  
$EndComp
$Comp
L PWR_FLAG #FLG?
U 1 1 5B126291
P 3500 2650
F 0 "#FLG?" H 3500 2745 30  0001 C CNN
F 1 "PWR_FLAG" H 3500 2830 30  0000 C CNN
F 2 "" H 3500 2650 60  0000 C CNN
F 3 "" H 3500 2650 60  0000 C CNN
	1    3500 2650
	1    0    0    -1  
$EndComp
Wire Wire Line
	3500 2650 3500 2750
Connection ~ 3500 2750
$EndSCHEMATC
